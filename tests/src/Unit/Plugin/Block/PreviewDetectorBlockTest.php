<?php

namespace Drupal\Tests\entity_reference_preview\Unit\Plugin\Block;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\entity_reference_preview\Form\PreviewActionsForm;
use Drupal\entity_reference_preview\Plugin\Block\PreviewDetectorBlock;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;

/**
 * @coversDefaultClass \Drupal\entity_reference_preview\Plugin\Block\PreviewDetectorBlock
 * @group entity_reference_preview
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class PreviewDetectorBlockTest extends UnitTestCase {

  use ProphecyTrait;
  /**
   * The system under test.
   *
   * @var \Drupal\entity_reference_preview\Plugin\Block\PreviewDetectorBlock
   */
  private $block;

  /**
   * The form builder.
   *
   * @var \Prophecy\Prophecy\ProphecySubjectInterface
   */
  private $formBuilder;

  /**
   * Is previewing?
   *
   * @var bool
   */
  private $isPreviewing;

  /**
   * Is previewing using a cookie?
   *
   * @var bool
   */
  private $previewingViaCookie;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $this->formBuilder = $this->prophesize(FormBuilderInterface::class);
    $this->isPreviewing = (bool) mt_rand(0, 1);
    $this->previewingViaCookie = (bool) mt_rand(0, 1);
    $this->block = new PreviewDetectorBlock(
      [],
      'entity_reference_preview_preview_detector',
      ['provider' => 'entity_reference_preview'],
      $this->formBuilder->reveal(),
      $this->isPreviewing,
      $this->previewingViaCookie
    );
  }

  /**
   * @covers ::build
   */
  public function testBuild() {
    $this->formBuilder
      ->buildForm(
        PreviewActionsForm::class,
        // Ensure that the argument is a form state with the correct values.
        Argument::allOf(
          Argument::type(FormStateInterface::class),
          Argument::which('getValues', [
            'is_previewing' => $this->isPreviewing,
            'previewing_via_cookie' => $this->previewingViaCookie,
          ])
        )
      )
      ->shouldBecalled($this->once());
    $this->block->build();
  }

  /**
   * @covers ::getCacheContexts
   */
  public function testGetCacheContexts() {
    $this->assertEquals(
      ['entity_reference_preview'],
      $this->block->getCacheContexts()
    );
  }

}
