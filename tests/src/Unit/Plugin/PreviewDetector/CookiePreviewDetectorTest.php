<?php

namespace Drupal\Tests\entity_reference_preview\Unit\Plugin\PreviewDetector;

use Drupal\entity_reference_preview\Plugin\PreviewDetector\CookiePreviewDetector;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;

/**
 * @coversDefaultClass \Drupal\entity_reference_preview\Plugin\PreviewDetector\CookiePreviewDetector
 * @group entity_reference_preview
 */
class CookiePreviewDetectorTest extends UnitTestCase {

  /**
   * Test the main method.
   *
   * @covers ::isPreviewing
   * @covers ::start
   * @covers ::stop
   */
  public function testIsPreviewing() {
    $request = new Request();
    $request->setSession(new Session(new MockArraySessionStorage()));
    $sut = new CookiePreviewDetector([], 'cookie', [], $request);
    $this->assertFalse($sut->isPreviewing($request));
    $sut->start();
    $this->assertTrue($sut->isPreviewing($request));
    $sut->stop();
    $this->assertFalse($sut->isPreviewing($request));
  }

}
