<?php

namespace Drupal\Tests\entity_reference_preview\Unit\Plugin\PreviewDetector;

use Drupal\Core\Entity\Enhancer\EntityRouteEnhancer;
use Drupal\Core\Routing\RouteObjectInterface;
use Drupal\node\Entity\Node;
use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\entity_reference_preview\Plugin\PreviewDetector\RenderedEntityPreviewDetector;
use Drupal\node\NodeInterface;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

/**
 * @coversDefaultClass \Drupal\entity_reference_preview\Plugin\PreviewDetector\RenderedEntityPreviewDetector
 * @group entity_reference_preview
 */
class RenderedEntityPreviewDetectorTest extends UnitTestCase {

  use ProphecyTrait;
  /**
   * Data provider for testIsPreviewing.
   *
   * @return bool[][]
   *   The data.
   */
  public function dataProviderIsPreviewing(): array {
    return [
      [TRUE, FALSE, FALSE, TRUE],
      [FALSE, FALSE, FALSE, FALSE],
      [FALSE, FALSE, TRUE, FALSE],
      [FALSE, TRUE, FALSE, FALSE],
      [FALSE, TRUE, TRUE, FALSE],
      [TRUE, FALSE, TRUE, FALSE],
      [TRUE, TRUE, FALSE, FALSE],
      [TRUE, TRUE, TRUE, FALSE],
    ];
  }

  /**
   * Test the main method.
   *
   * @param bool $latest
   *   Is the latest revision?.
   * @param bool $default
   *   Is the default revision?.
   * @param bool $new
   *   Is a new entity?.
   * @param bool $expected
   *   Is it in preview mode?.
   *
   * @covers ::isPreviewing
   * @dataProvider dataProviderIsPreviewing
   */
  public function testIsPreviewing(bool $latest, bool $default, bool $new, bool $expected) {
    $request = new Request();
    $node = $this->prophesize(NodeInterface::class);
    $node->isLatestTranslationAffectedRevision()->willReturn($latest);
    $node->isDefaultRevision()->willReturn($default);
    $node->isNew()->willReturn($new);
    $node->getEntityTypeId()->willReturn('node');
    $request->attributes->set('node', $node->reveal());
    $request->attributes->set(RouteObjectInterface::ROUTE_OBJECT, new Route('/node/{node}/preview'));
    $sut = new RenderedEntityPreviewDetector([], 'rendered_entity', []);
    $this->assertSame($expected, $sut->isPreviewing($request));
  }

  /**
   * Test the main method.
   *
   * @covers ::isPreviewing
   */
  public function testIsPreviewingNoEntity() {
    $request = new Request();
    $request->attributes->set('node', mt_rand());
    $sut = new RenderedEntityPreviewDetector([], 'rendered_entity', []);
    $this->assertFalse($sut->isPreviewing($request));
  }

  /**
   * Test the main method.
   *
   * @covers ::isPreviewing
   */
  public function testIsPreviewingMultiple() {
    $request = new Request();
    $node1 = $this->prophesize(NodeInterface::class);
    $node1->isLatestTranslationAffectedRevision()->willReturn(TRUE);
    $node1->isDefaultRevision()->willReturn(FALSE);
    $node1->isNew()->willReturn(FALSE);
    $node2 = $this->prophesize(NodeInterface::class);
    $node2->isLatestTranslationAffectedRevision()->willReturn(TRUE);
    $node2->isDefaultRevision()->willReturn(FALSE);
    $node2->isNew()->willReturn(FALSE);
    $request->attributes->set('node1', $node1);
    $request->attributes->set('node2', $node2);
    $sut = new RenderedEntityPreviewDetector([], 'rendered_entity', []);
    $this->assertFalse($sut->isPreviewing($request));
  }

}
