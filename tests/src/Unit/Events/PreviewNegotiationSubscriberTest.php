<?php

namespace Drupal\Tests\entity_reference_preview\Unit\Events;

use Prophecy\PhpUnit\ProphecyTrait;
use Drupal\entity_reference_preview\Events\PreviewNegotiationSubscriber;
use Drupal\entity_reference_preview\PreviewDetectorPluginManager;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\RequestEvent;

/**
 * @coversDefaultClass \Drupal\entity_reference_preview\Events\PreviewNegotiationSubscriber
 * @group entity_reference_preview
 *
 * @SuppressWarnings(PHPMD.StaticAccess)
 */
class PreviewNegotiationSubscriberTest extends UnitTestCase {

  use ProphecyTrait;
  /**
   * The system under test.
   *
   * @var \Drupal\entity_reference_preview\Events\PreviewNegotiationSubscriber
   */
  private $eventSubscriber;

  /**
   * The form builder.
   *
   * @var \Prophecy\Prophecy\ProphecySubjectInterface
   */
  private $previewDetector;

  /**
   * Data provider for testSetPreview.
   *
   * @return bool[][]
   *   The data.
   */
  public function dataProviderSetPreview() {
    return [['foo'], [NULL]];
  }

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    $this->previewDetector = $this->prophesize(PreviewDetectorPluginManager::class);
    $this->eventSubscriber = new PreviewNegotiationSubscriber(
      $this->previewDetector->reveal()
    );
  }

  /**
   * @covers ::setPreview
   * @dataProvider dataProviderSetPreview
   */
  public function testSetPreview($active_detector) {
    $this->previewDetector
      ->isPreviewing(Argument::type(Request::class))
      ->shouldBecalled($this->once());
    $this->previewDetector
      ->activeDetector(Argument::type(Request::class))
      ->willReturn($active_detector)
      ->shouldBecalled($this->once());
    $request = new Request();
    $event = $this->prophesize(RequestEvent::class);
    $event->getRequest()->willReturn($request)->shouldBeCalled($this->once());
    $this->eventSubscriber->setPreview($event->reveal());
    $flag = $request->attributes
      ->get(PreviewNegotiationSubscriber::IS_PREVIEWING);
    $this->assertSame((bool) $active_detector, $flag);
    $actual = $request->attributes
      ->get(PreviewNegotiationSubscriber::ACTIVE_DETECTOR);
    $this->assertSame($active_detector, $actual);
  }

}
