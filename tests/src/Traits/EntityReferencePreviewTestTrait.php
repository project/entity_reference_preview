<?php

namespace Drupal\Tests\entity_reference_preview\Traits;

use Drupal\node\Entity\Node;
use Drupal\Tests\field\Traits\EntityReferenceTestTrait;

/**
 * Trait EntityReferencePreviewTestTrait.
 */
trait EntityReferencePreviewTestTrait {

  use EntityReferenceTestTrait;

  /**
   * Adds an entity_reference field to a content type, and set our formatter.
   *
   * We assume this content type will be used both as host and target in the
   * entity_reference field. We also set the formatter with no additional
   * settings.
   *
   * @param string $content_type_id
   *   Content type ID to be used.
   * @param string $field_name
   *   Field name to be used.
   * @param string $field_label
   *   (optional) The field label to use. Defaults to the field machine name.
   */
  protected function addErFieldToContentType($content_type_id, $field_name, $field_label = NULL) {
    $handler_settings = ['target_bundles' => [$content_type_id => $content_type_id]];
    $this->createEntityReferenceField('node', $content_type_id, $field_name, $field_label ?? $field_name, 'node', 'default:node', $handler_settings, -1);

    // Set the view display to use our formatter.
    /** @var \Drupal\Core\Entity\Display\EntityViewDisplayInterface $view_display */
    $view_display = \Drupal::service('entity_display.repository')->getViewDisplay('node', $content_type_id);
    $view_display->setComponent($field_name, ['type' => 'entity_reference_entity_view_preview']);
    $view_display->save();
  }

  /**
   * Helper to generate a node.
   *
   * @param string $type
   *   The node type to generate.
   * @param string $title
   *   The title to be used.
   * @param array $fields
   *   (optional) An associative array with field values, where keys are field
   *   names and values their values. Defaults to empty.
   *
   * @return \Drupal\node\NodeInterface
   *   An UNSAVED node object.
   */
  protected function generateNode($type, $title, array $fields = []) {
    $values = [
      'type' => $type,
      'title' => $title,
      'uid' => 1,
    ];
    foreach ($fields as $field_name => $field_value) {
      $values[$field_name] = $field_value;
    }
    return Node::create($values);
  }

}
