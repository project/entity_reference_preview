<?php

namespace Drupal\Tests\entity_reference_preview\Functional;

use Drupal\Tests\entity_reference_preview\Traits\EntityReferencePreviewTestTrait;
use Drupal\views\Tests\ViewTestData;

/**
 * Tests preview on entity_reference integration.
 *
 * @group entity_reference_preview
 */
class EntityReferencePreviewViewsTest extends EntityReferencePreviewFunctionalTestBase {

  use EntityReferencePreviewTestTrait;

  /**
   * {@inheritdoc}
   */
  public static $testViews = ['node_test'];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = ['views_test_config']): void {
    parent::setUp($import_test_views);
    $this->drupalLogin($this->adminUser);

    // Prepare the content type we will be using.
    $this->createContentTypeFromUi('Article', 'article', TRUE);
    $this->grantUserPermissionToCreateContentOfType($this->adminUser, 'article');
    $this->enableViewsTestModule();
    ViewTestData::createTestViews(get_class($this), ['entity_reference_preview_tests']);
  }

  /**
   * Test the preview formatter when checking the "latest" entity route.
   */
  public function testEntityReferencePreview() {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();
    $this->generateTestContent();

    // Enable preview mode and ensure that the drafts are previewed.
    $this->enablePreviewModeThroughUi();
    $this->drupalGet('/nodes-with-preview');
    $assert_session->titleEquals('Node test | Drupal');
    // Check that the entity shown is the published version.
    $assert_session->linkExists('Node with forward revision (draft)');
    $this->disablePreviewModeThroughUi();
    $this->drupalGet('/nodes-with-preview');
    $assert_session->titleEquals('Node test | Drupal');
    // Check that the entity shown is the published version.
    $assert_session->linkExists('Node with forward revision (published)');
  }

  /**
   * Test the preview formatter when checking the "latest" entity route.
   */
  public function testEntityReferencePreviewIndicator() {
    $assert_session = $this->assertSession();
    $this->generateTestContent();

    // Test that the view does not contain the indicators when the view
    // configuration is not turned on.
    $this->drupalGet('/nodes-without-preview');
    $assert_session->titleEquals('Node test (no preview) | Drupal');
    $assert_session->elementNotExists('css', '.entity-reference-preview__has-draft');
    // Test that views with the setting turned on show the draft indicator. This
    // also ensures that there are no stale caches with entities with an
    // indicator and without.
    $this->drupalGet('/nodes-with-preview');
    $assert_session->titleEquals('Node test | Drupal');
    $assert_session->elementsCount('css', '.entity-reference-preview__has-draft', 1);
    // Check that the entity shown is the published version.
    $assert_session->linkExists('Node with forward revision (published');
    // Ensure that even for views with the setting turned on, the indicator is
    // absent when the configuration for the indicator is off.
    $this->disableIndicatorThroughUi();
    $this->drupalGet('/nodes-with-preview');
    $assert_session->titleEquals('Node test | Drupal');
    $assert_session->elementNotExists('css', '.entity-reference-preview__has-draft');
  }

  /**
   * Generates the test content.
   *
   * Create some nodes.
   * 1. A published node with a forward revision ($node1).
   * 2. A node that was never published ($node2).
   * 3. A published node with a prior published version ($node3).
   *
   * @return \Drupal\node\NodeInterface[]
   *   The nodes.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function generateTestContent(): array {
    $assert_session = $this->assertSession();
    $node1 = $this->generateNode('article', 'Node with forward revision (published)');
    $node1->moderation_state = 'published';
    $node1->save();
    $node1->setTitle('Node with forward revision (draft)');
    $node1->moderation_state = 'draft';
    $node1->setNewRevision(TRUE);
    $node1->save();
    $this->drupalGet("/node/{$node1->id()}");
    $assert_session->titleEquals('Node with forward revision (published) | Drupal');
    $this->drupalGet("/node/{$node1->id()}/latest");
    $assert_session->titleEquals('Node with forward revision (draft) | Drupal');
    $node2 = $this->generateNode('article', 'Node with drafts only (1)');
    $node2->moderation_state = 'draft';
    $node2->setNewRevision(TRUE);
    $node2->save();
    $node2->setTitle('Node with drafts only (2)');
    $node2->moderation_state = 'draft';
    $node2->setNewRevision(TRUE);
    $node2->save();
    $this->drupalGet("/node/{$node2->id()}");
    $assert_session->titleEquals('Node with drafts only (2) | Drupal');
    $this->drupalGet("/node/{$node2->id()}/latest");
    $assert_session->statusCodeEquals(403);
    $node3 = $this->generateNode('article', 'Node with several published (1)');
    $node3->moderation_state = 'published';
    $node3->setNewRevision(TRUE);
    $node3->save();
    $node3->setTitle('Node with several published (2)');
    $node3->moderation_state = 'published';
    $node3->setNewRevision(TRUE);
    $node3->save();
    $this->drupalGet("/node/{$node3->id()}");
    $assert_session->titleEquals('Node with several published (2) | Drupal');
    $this->drupalGet("/node/{$node3->id()}/latest");
    $assert_session->statusCodeEquals(403);
    return [$node1, $node2, $node3];
  }

  /**
   * Test the preview indicator when checking the canonical entity route.
   */
  public function testEntityReferencePreviewViewsAccess() {
    $assert_session = $this->assertSession();
    $nodes = $this->generateTestContent();
    \Drupal::state()->set('entity_reference_preview_test.denied_vids', [
      $nodes[0]->getRevisionId(),
    ]);

    // Enable preview mode and ensure that the drafts are previewed.
    $this->enablePreviewModeThroughUi();
    $this->drupalGet('/nodes-with-preview');
    $assert_session->titleEquals('Node test | Drupal');
    // Check that the disallowed entities do not render. The first article would
    // be a draft, but it falls back to the published version because access is
    // denied to the latest draft.
    $assert_session->linkExists('Node with forward revision (published)');
    $assert_session->linkExists('Node with several published (2)');
  }

}
