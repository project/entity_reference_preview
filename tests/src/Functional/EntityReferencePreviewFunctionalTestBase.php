<?php

namespace Drupal\Tests\entity_reference_preview\Functional;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\content_moderation\Traits\ContentModerationTestTrait;
use Drupal\Tests\views\Functional\ViewTestBase;
use Drupal\user\Entity\Role;

/**
 * Defines a base class for our functional tests.
 */
abstract class EntityReferencePreviewFunctionalTestBase extends ViewTestBase {

  use ContentModerationTestTrait;

  /**
   * View IDs to install for testing purposes.
   *
   * @var string[]
   */
  public static $testViews = [];

  /**
   * {@inheritdoc}
   */
  protected $profile = 'testing';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * An admin user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $adminUser;

  /**
   * Permissions to grant the admin user.
   *
   * @var array
   */
  protected $adminPermissions = [
    'administer workflows',
    'access administration pages',
    'administer content types',
    'administer nodes',
    'view latest version',
    'view any unpublished content',
    'access content overview',
    'use editorial transition create_new_draft',
    'use editorial transition publish',
    'use editorial transition archive',
    'use editorial transition archived_draft',
    'use editorial transition archived_published',
    'administer entity_reference_preview configuration',
    'view entity_reference_preview indicator',
    'request entity_reference_preview preview',
  ];

  /**
   * The editorial workflow entity.
   *
   * @var \Drupal\workflows\Entity\Workflow
   */
  protected $workflow;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_moderation',
    'block',
    'node',
    'entity_reference_preview',
    'entity_reference_preview_tests',
    'views',
    'views_test_config',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = ['views_test_config']): void {
    parent::setUp($import_test_views);
    $this->workflow = $this->createEditorialWorkflow();
    $this->adminUser = $this->drupalCreateUser($this->adminPermissions);
    $this->drupalPlaceBlock('local_tasks_block', ['id' => 'tabs_block']);
    $this->drupalPlaceBlock('page_title_block');
    $this->drupalPlaceBlock('local_actions_block', ['id' => 'actions_block']);
  }

  /**
   * Creates a content type from the UI.
   *
   * @param string $content_type_name
   *   Content type human name.
   * @param string $content_type_id
   *   Machine name.
   * @param bool $moderated
   *   TRUE if the content type should be moderated.
   * @param string $workflow_id
   *   The workflow to attach to the bundle.
   */
  protected function createContentTypeFromUi($content_type_name, $content_type_id, $moderated = FALSE, $workflow_id = 'editorial') {
    $this->drupalGet('admin/structure/types');
    $this->clickLink('Add content type');

    // Check that the 'Create new revision' checkbox is checked and disabled.
    $this->assertSession()->checkboxChecked('options[revision]');

    $edit = [
      'name' => $content_type_name,
      'type' => $content_type_id,
    ];
    $this->submitForm($edit, t('Save content type'));

    // Check the content type has been set to create new revisions.
    $this->assertTrue(NodeType::load($content_type_id)->shouldCreateNewRevision());

    if ($moderated) {
      $this->enableModerationThroughUi($content_type_id, $workflow_id);
    }
  }

  /**
   * Enable moderation for a specified content type, using the UI.
   *
   * @param string $content_type_id
   *   Machine name.
   * @param string $workflow_id
   *   The workflow to attach to the bundle.
   */
  public function enableModerationThroughUi($content_type_id, $workflow_id = 'editorial') {
    $this->drupalGet('/admin/config/workflow/workflows');
    $this->assertSession()->linkByHrefExists('admin/config/workflow/workflows/manage/' . $workflow_id);
    $edit['bundles[' . $content_type_id . ']'] = TRUE;
    $this->drupalGet('admin/config/workflow/workflows/manage/' . $workflow_id . '/type/node');
    $this->submitForm($edit, t('Save'));
    // Ensure the parent environment is up-to-date.
    // @see content_moderation_workflow_insert()
    \Drupal::service('entity_type.bundle.info')->clearCachedBundles();
    \Drupal::service('entity_field.manager')->clearCachedFieldDefinitions();
    /** @var \Drupal\Core\Routing\RouteBuilderInterface $router_builder */
    $router_builder = $this->container->get('router.builder');
    $router_builder->rebuildIfNeeded();
  }

  /**
   * Grants given user permissions to create content of given type.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User to grant permission to.
   * @param string $content_type_id
   *   Content type ID.
   */
  protected function grantUserPermissionToCreateContentOfType(AccountInterface $account, $content_type_id) {
    $role_ids = $account->getRoles(TRUE);
    /* @var \Drupal\user\RoleInterface $role */
    $role_id = reset($role_ids);
    $role = Role::load($role_id);
    $role->grantPermission(sprintf('create %s content', $content_type_id));
    $role->grantPermission(sprintf('edit any %s content', $content_type_id));
    $role->grantPermission(sprintf('delete any %s content', $content_type_id));
    $role->save();
  }

  /**
   * Enables the indicator setting.
   */
  protected function enableIndicatorThroughUi() {
    $edit = ['draft_indicator' => '1'];
    $url = Url::fromRoute('entity_reference_preview.settings');
    $this->drupalGet($url);
    $this->submitForm($edit, t('Save configuration'));
  }

  /**
   * Disables the indicator setting.
   */
  protected function disableIndicatorThroughUi() {
    $edit = ['draft_indicator' => '0'];
    $url = Url::fromRoute('entity_reference_preview.settings');
    $this->drupalGet($url);
    $this->submitForm($edit, t('Save configuration'));
  }

  /**
   * Enables the indicator setting.
   */
  protected function enablePreviewModeThroughUi() {
    $url = Url::fromRoute('entity_reference_preview.controls');
    $this->drupalGet($url);
    $this->submitForm([], t('Start'));
  }

  /**
   * Enables the indicator setting.
   */
  protected function disablePreviewModeThroughUi() {
    $url = Url::fromRoute('entity_reference_preview.controls');
    $this->drupalGet($url);
    $this->submitForm([], t('Stop'));
  }

}
