<?php

namespace Drupal\Tests\entity_reference_preview\Functional;

use Drupal\Tests\entity_reference_preview\Traits\EntityReferencePreviewTestTrait;

/**
 * Tests preview on entity_reference integration.
 *
 * @group entity_reference_preview
 */
class EntityReferencePreviewTest extends EntityReferencePreviewFunctionalTestBase {

  use EntityReferencePreviewTestTrait;

  /**
   * {@inheritdoc}
   */
  protected function setUp($import_test_views = TRUE, $modules = ['views_test_config']): void {
    parent::setUp($import_test_views);
    $this->drupalLogin($this->adminUser);

    // Prepare the content type we will be using.
    $this->createContentTypeFromUi('Article', 'article', TRUE);
    $this->grantUserPermissionToCreateContentOfType($this->adminUser, 'article');
    $this->addErFieldToContentType('article', 'related_content', 'Related content');
  }

  /**
   * Test the preview formatter when checking the "latest" entity route.
   */
  public function testEntityReferencePreviewFormatter() {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    // Create a target node.
    $target_node = $this->generateNode('article', 'Target node foo');
    $target_node->moderation_state = 'published';
    $target_node->save();
    $this->drupalGet("/node/{$target_node->id()}");
    $assert_session->titleEquals('Target node foo | Drupal');


    // Create a host node pointing to the target node.
    $host_node = $this->generateNode('article', 'Host node foo');
    $host_node->related_content = ['target_id' => $target_node->id()];
    $host_node->moderation_state = 'published';
    $host_node->save();
    $this->drupalGet("/node/{$host_node->id()}");
    $assert_session->titleEquals('Host node foo | Drupal');
    $assert_session->linkExists('Target node foo');

    // Create forward revisions of both nodes.
    $target_node->setTitle('Target node bar');
    $target_node->moderation_state = 'draft';
    $target_node->setNewRevision(TRUE);
    $target_node->save();
    $this->drupalGet("/node/{$target_node->id()}/latest");
    $assert_session->titleEquals('Target node bar | Drupal');

    $host_node->setTitle('Host node bar');
    $host_node->moderation_state = 'draft';
    $host_node->setNewRevision(TRUE);
    $host_node->save();

    // Check that our formatter displays the default revision on the canonical
    // route, and the latest target on the preview route.
    $this->drupalGet("/node/{$host_node->id()}");
    $assert_session->titleEquals('Host node foo | Drupal');
    $assert_session->linkExists('Target node foo');

    $this->drupalGet("/node/{$host_node->id()}/latest");
    $assert_session->titleEquals('Host node bar | Drupal');
    $assert_session->linkExists('Target node bar');

    // Re-request the non-latest to ensure there is no cache poisoning.
    $this->drupalGet("/node/{$host_node->id()}");
    $assert_session->titleEquals('Host node foo | Drupal');
    $assert_session->linkExists('Target node foo');

    // Publish the target through the UI, check the display updates accordingly.
    $this->drupalGet("/node/{$target_node->id()}/edit");
    $page->fillField('title[0][value]', 'Target node baz');
    $page->selectFieldOption('moderation_state[0][state]', 'published');
    $page->pressButton('edit-submit');
    $assert_session->pageTextContains('Article Target node baz has been updated');
    $this->drupalGet("/node/{$host_node->id()}");
    $assert_session->titleEquals('Host node foo | Drupal');
    $assert_session->linkExists('Target node baz');
    $this->drupalGet("/node/{$host_node->id()}/latest");
    $assert_session->titleEquals('Host node bar | Drupal');
    $assert_session->linkExists('Target node baz');
    // Start preview using the button, not the latest.
    $this->enablePreviewModeThroughUi();
    $this->drupalGet("/node/{$host_node->id()}");
    $assert_session->titleEquals('Host node foo | Drupal');
    $assert_session->linkExists('Target node baz');
    $this->disablePreviewModeThroughUi();
  }

  /**
   * Test the preview indicator when checking the canonical entity route.
   */
  public function testEntityReferencePreviewFormatterIndicator() {
    $page = $this->getSession()->getPage();
    $assert_session = $this->assertSession();

    /** @var \Drupal\node\NodeStorageInterface $storage */
    $storage = \Drupal::entityTypeManager()->getStorage('node');

    // The Editor can't see the preview indicator. $this->adminUser can.
    $editor_user = $this->createUser([
      'view latest version',
      'view any unpublished content',
      'use editorial transition create_new_draft',
      'use editorial transition publish',
      'use editorial transition archive',
      'use editorial transition archived_draft',
      'use editorial transition archived_published',
    ]);
    $this->grantUserPermissionToCreateContentOfType($editor_user, 'article');
    $this->drupalLogin($editor_user);

    // Create a target node.
    $target_node = $this->generateNode('article', 'Target node foo');
    $target_node->moderation_state = 'published';
    $target_node->save();
    $this->drupalGet("/node/{$target_node->id()}");
    $assert_session->titleEquals('Target node foo | Drupal');

    // Create a host node pointing to the target node.
    $host_node = $this->generateNode('article', 'Host node foo');
    $host_node->related_content = ['target_id' => $target_node->id()];
    $host_node->moderation_state = 'published';
    $host_node->save();
    $this->drupalGet("/node/{$host_node->id()}");
    $assert_session->titleEquals('Host node foo | Drupal');
    $assert_session->linkExists('Target node foo');
    // No indicator is present for any of the users.
    $assert_session->elementNotExists('css', '.node__content .field--name-related-content .entity-reference-preview__has-draft');
    $this->drupalLogin($this->adminUser);
    $this->drupalGet("/node/{$host_node->id()}");
    $assert_session->titleEquals('Host node foo | Drupal');
    $assert_session->linkExists('Target node foo');
    $assert_session->elementNotExists('css', '.node__content .field--name-related-content .entity-reference-preview__has-draft');

    // Create forward revisions of both nodes.
    $target_node->setTitle('Target node bar');
    $target_node->moderation_state = 'draft';
    $target_node->setNewRevision(TRUE);
    $target_node->save();
    $this->drupalGet("/node/{$target_node->id()}/latest");
    $assert_session->titleEquals('Target node bar | Drupal');

    $host_node->setTitle('Host node bar');
    $host_node->moderation_state = 'draft';
    $host_node->setNewRevision(TRUE);
    $host_node->save();

    // Check the indicator is there when expected.
    $this->enableIndicatorThroughUi();
    $this->drupalGet("/node/{$host_node->id()}");
    $assert_session->titleEquals('Host node foo | Drupal');
    $assert_session->linkExists('Target node foo');
    // The admin user can see the indicator, and it has the text we expect.
    $indicator = $assert_session->elementExists('css', '.entity-reference-preview__has-draft');
    $target_id = $target_node->id();
    $target_default_vid = $storage->loadUnchanged($target_id)->getRevisionId();
    $target_last_vid = $storage->getLatestRevisionId($target_id);
    $expected = "Content entity (id: {$target_id}, version: {$target_default_vid}) has new draft (version: {$target_last_vid}): \"Target node foo\"";
    $this->assertSame($expected, $indicator->getAttribute('title'));
    // If the indicator setting is disabled, the admin does not see it.
    $this->disableIndicatorThroughUi();
    $this->drupalGet("/node/{$host_node->id()}");
    $assert_session->elementNotExists('css', '.entity-reference-preview__has-draft');
    // Re-enable the indicator and carry check that there are no stale caches.
    $this->enableIndicatorThroughUi();
    $this->drupalGet("/node/{$host_node->id()}");
    $assert_session->elementExists('css', '.entity-reference-preview__has-draft');

    // Editor still not sees it.
    $this->drupalLogin($editor_user);
    $this->drupalGet("/node/{$host_node->id()}");
    $assert_session->titleEquals('Host node foo | Drupal');
    $assert_session->linkExists('Target node foo');
    $assert_session->elementNotExists('css', '.entity-reference-preview__has-draft');

    // Publish the target and verify the indicator is no longer there.
    $target_node->setTitle('Target node baz');
    $target_node->moderation_state = 'published';
    $target_node->setNewRevision(TRUE);
    $target_node->save();

    $this->drupalLogin($this->adminUser);
    $this->drupalGet("/node/{$host_node->id()}");
    $assert_session->titleEquals('Host node foo | Drupal');
    $assert_session->linkExists('Target node baz');
    $assert_session->elementNotExists('css', '.entity-reference-preview__has-draft');
  }

  /**
   * Test the preview indicator when checking the canonical entity route.
   */
  public function testEntityReferencePreviewFormatterAccess() {
    $assert_session = $this->assertSession();

    // Create a target node.
    $target_node1 = $this->generateNode('article', 'Target node 1 foo');
    $target_node1->moderation_state = 'published';
    $target_node1->save();
    $target_node2 = $this->generateNode('article', 'Target node 2 foo');
    $target_node2->moderation_state = 'published';
    $target_node2->save();


    // Create a host node pointing to the target node.
    $host_node = $this->generateNode('article', 'Host node foo');
    $host_node->related_content = [
      ['target_id' => $target_node1->id()],
      ['target_id' => $target_node2->id()],
    ];
    $host_node->moderation_state = 'published';
    $host_node->save();
    $this->drupalGet("/node/{$host_node->id()}");
    $assert_session->titleEquals('Host node foo | Drupal');
    $assert_session->linkExists('Target node 1 foo');

    // Create forward revisions of the host and one target.
    $target_node1->setTitle('Target node 1 bar');
    $target_node1->moderation_state = 'draft';
    $target_node1->setNewRevision(TRUE);
    $target_node1->save();
    $host_node->setTitle('Host node bar');
    $host_node->moderation_state = 'draft';
    $host_node->setNewRevision(TRUE);
    $host_node->save();

    // Check that our formatter displays the latest target on the preview route.
    $this->drupalGet("/node/{$host_node->id()}/latest");
    $assert_session->titleEquals('Host node bar | Drupal');
    $assert_session->linkExists('Target node 1 bar');
    $assert_session->linkExists('Target node 2 foo');

    // Check that inaccessible unpublished versions are not accidentally
    // exposed.
    \Drupal::state()->set('entity_reference_preview_test.denied_vids', [
      $target_node1->getRevisionId(),
      $target_node2->getRevisionId(),
    ]);
    $this->drupalGet("/node/{$host_node->id()}/latest");
    $assert_session->titleEquals('Host node bar | Drupal');
    $assert_session->elementNotExists('css', '.node__content .field--name-related-content');
    \Drupal::state()->set('entity_reference_preview_test.denied_vids', []);
  }

}
