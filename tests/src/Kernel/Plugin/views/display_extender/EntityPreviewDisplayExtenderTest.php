<?php

namespace Drupal\Tests\entity_reference_preview\Kernel\Plugin\views\display_extender;

use Drupal\Core\Form\FormState;
use Drupal\entity_reference_preview\Plugin\views\display_extender\EntityPreviewDisplayExtender;
use Drupal\KernelTests\KernelTestBase;

/**
 * @coversDefaultClass \Drupal\entity_reference_preview\Plugin\views\display_extender\EntityPreviewDisplayExtender
 * @group entity_reference_preview
 */
class EntityPreviewDisplayExtenderTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'views'];

  /**
   * Data provider for testOptionsSummary.
   *
   * @return array
   *   The data.
   */
  public function dataProviderOptionsSummary() {
    return [
      [FALSE, 'Disabled.'],
      [TRUE, 'Enabled while preview mode is on.'],
    ];
  }

  /**
   * Tests the optionsSummary method.
   *
   * @param bool $enabled
   *   Is enabled?
   * @param string $ui_text
   *   The UI text for the view.
   *
   * @covers ::optionsSummary
   * @dataProvider dataProviderOptionsSummary
   */
  public function testOptionsSummary(bool $enabled, string $ui_text) {
    $categories = [];
    $options = [];
    $extender = new EntityPreviewDisplayExtender([], 'entity_preview', []);
    $extender->options = ['entity_reference_preview_enable' => $enabled];
    $extender->optionsSummary($categories, $options);
    $this->assertEquals([
      'entity_reference_preview' => [
        'title' => 'Entity Preview',
        'column' => 'second',
        'build' => ['#weight' => -100],
      ],
    ], $categories);
    $this->assertEquals([
      'entity_reference_preview_enable' => [
        'category' => 'entity_reference_preview',
        'title' => 'Entity preview',
        'value' => $ui_text,
      ],
    ], $options);
  }

  /**
   * Test for the buildOptionsForm method.
   *
   * @covers ::buildOptionsForm
   */
  public function testBuildOptions() {
    $form_state = new FormState();;
    $form_state->set('section', 'entity_reference_preview_enable');
    $extender = new EntityPreviewDisplayExtender([], 'entity_preview', []);
    $enabled = (bool) mt_rand(0, 1);
    $extender->options = ['entity_reference_preview_enable' => $enabled];
    $form = [];

    $extender->buildOptionsForm($form, $form_state);
    $this->assertEquals([
      '#title' => 'Entity preview',
      'entity_reference_preview_enable' => [
        '#title' => 'Entity preview',
        '#type' => 'checkbox',
        '#description' => 'Check to render the latest revisions on entities when the preview mode is active.',
        '#default_value' => $enabled,
      ],
    ], $form);
  }

  /**
   * Test for the submitOptionsForm method.
   *
   * @covers ::submitOptionsForm
   */
  public function testSubmitOptionsForm() {
    $form_state = new FormState();;
    $form_state->set('section', 'entity_reference_preview_enable');
    $form_state->setValue('entity_reference_preview_enable', TRUE);
    $extender = new EntityPreviewDisplayExtender([], 'entity_preview', []);
    $extender->options = ['entity_reference_preview_enable' => FALSE];
    $form = [];
    $extender->submitOptionsForm($form, $form_state);
    $this->assertSame(TRUE, $extender->options['entity_reference_preview_enable']);
  }

  /**
   * Test for the defaultableSections method.
   *
   * @covers ::defaultableSections
   */
  public function testDefaultableSections() {
    $extender = new EntityPreviewDisplayExtender([], 'entity_preview', []);
    $sections = [];
    $extender->defaultableSections($sections);
    $this->assertEquals([
      'entity_reference_preview_enable' => ['entity_reference_preview_enable'],
    ], $sections);
  }

}
