<?php

namespace Drupal\entity_reference_preview\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines preview_detector annotation object.
 *
 * @Annotation
 */
class PreviewDetector extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $title;

  /**
   * The description of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $description;

}
