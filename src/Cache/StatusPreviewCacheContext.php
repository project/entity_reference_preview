<?php

namespace Drupal\entity_reference_preview\Cache;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;
use Drupal\entity_reference_preview\PreviewDetectorInterface;
use Drupal\entity_reference_preview\PreviewDetectorPluginManager;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Defines the StatePreviewCacheContext service, for "with preview" caching.
 *
 * Cache context ID: 'entity_reference_preview'.
 */
class StatusPreviewCacheContext implements CacheContextInterface {

  /**
   * The plugin manager.
   *
   * @var \Drupal\entity_reference_preview\PreviewDetectorPluginManager
   */
  private $manager;

  /**
   * The request object.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private $request;

  /**
   * StatePreviewCacheContext constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\entity_reference_preview\PreviewDetectorPluginManager $manager
   *   The plugin manager.
   */
  public function __construct(
    RequestStack $request_stack,
    PreviewDetectorPluginManager $manager
  ) {
    $this->request = $request_stack->getCurrentRequest();
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Entity Reference Preview state cache context');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    // Digest the status of all the plugins.
    $instances = array_map(function (array $definition) {
      return $this->manager->createInstance($definition['id']);
    }, $this->manager->getDefinitions());
    $res = array_map(
      function (PreviewDetectorInterface $instance) {
        return sprintf(
          '%s:%s',
          $instance->label(),
          (int) $instance->isPreviewing($this->request)
        );
      },
      $instances
    );
    return implode('|', $res);
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return (new CacheableMetadata())
      ->setCacheTags(['config:entity_reference_preview.settings']);
  }

}
