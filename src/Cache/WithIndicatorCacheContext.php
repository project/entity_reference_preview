<?php

namespace Drupal\entity_reference_preview\Cache;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Cache\Context\CacheContextInterface;

/**
 * Cache context to render entities with the indicator.
 *
 * Cache context ID: 'entity_reference_preview_with_indicator'.
 */
class WithIndicatorCacheContext implements CacheContextInterface {

  /**
   * {@inheritdoc}
   */
  public static function getLabel() {
    return t('Entity with preview indicator');
  }

  /**
   * {@inheritdoc}
   */
  public function getContext() {
    return 'entity_with_indicator';
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheableMetadata() {
    return (new CacheableMetadata())
      ->setCacheTags(['config:entity_reference_preview.settings']);
  }

}
