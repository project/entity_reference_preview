<?php

namespace Drupal\entity_reference_preview\Plugin\PreviewDetector;

use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\entity_reference_preview\PreviewDetectorPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Plugin implementation of the preview_detector.
 *
 * @PreviewDetector(
 *   id = "cookie",
 *   label = @Translation("Cookie"),
 *   description = @Translation("Detects the preview state on a value in a cookie.")
 * )
 */
class CookiePreviewDetector extends PreviewDetectorPluginBase implements ContainerFactoryPluginInterface {

  const COOKIE_NAME = 'entity_reference_preview.status';
  const STARTED = 'previewing';

  /**
   * The state service.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private $request;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Request $request) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->request = $request;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $request = $container->get('request_stack')->getCurrentRequest();
    return new static($configuration, $plugin_id, $plugin_definition, $request);
  }

  /**
   * {@inheritdoc}
   */
  public function isPreviewing(Request $request): bool {
    return $request->hasSession() && $request->getSession()->get(static::COOKIE_NAME) === static::STARTED;
  }

  /**
   * Starts previewing.
   */
  public function start(): void {
    $this->request->getSession()->set(static::COOKIE_NAME, static::STARTED);
  }

  /**
   * Stops previewing.
   */
  public function stop() {
    $this->request->getSession()->remove(static::COOKIE_NAME);
  }

}
