<?php

namespace Drupal\entity_reference_preview\Plugin\PreviewDetector;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\entity_reference_preview\PreviewDetectorPluginBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Route;

/**
 * Plugin implementation of the preview_detector.
 *
 * @PreviewDetector(
 *   id = "rendered_entity",
 *   label = @Translation("Rendered Entity"),
 *   description = @Translation("Detects the preview state based on the route-level rendered entity.")
 * )
 */
class RenderedEntityPreviewDetector extends PreviewDetectorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function isPreviewing(Request $request): bool {
    $entity = $this->extractEntityFromRequest($request);
    if (!$entity) {
      return FALSE;
    }
    $route = $request->attributes->get('_route_object');
    return $this->routeIsPreviewing($route, $entity->getEntityTypeId()) ||
      $this->entityIsPreviewing($entity);
  }

  /**
   * Extracts the route-level entity being rendered in this request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request that potentially contains the rendered entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface|null
   *   The entity associated to the request.
   */
  private function extractEntityFromRequest(Request $request): ?ContentEntityInterface {
    $params = array_filter($request->attributes->all(), function ($key) {
      return ($key[0] ?? '') !== '_';
    }, ARRAY_FILTER_USE_KEY);
    $publishable_entity_params = array_filter($params, function ($param) {
      return $param instanceof ContentEntityInterface
        && $param instanceof EntityPublishedInterface;
    });
    if (count($publishable_entity_params) !== 1) {
      // We guess that the entity being rendered is the only one in the
      // parameters, otherwise we do not know which one to use.
      return NULL;
    }
    return reset($publishable_entity_params);
  }

  /**
   * Checks the route for "load latest revision" parameter flag.
   *
   * @param \Symfony\Component\Routing\Route $route
   *   The route for the current request.
   * @param string $entity_type_id
   *   The ID of entity type for the route parameter.
   *
   * @return bool
   *   TRUE if the route is for a preview.
   */
  private function routeIsPreviewing(Route $route, string $entity_type_id): bool {
    $route_params = $route->getOption('parameters');
    return $route_params[$entity_type_id]['load_latest_revision'] ?? FALSE;
  }

  /**
   * The entity preview function.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity to check.
   *
   * @return bool
   *   TRUE if the entity is for a preview.
   */
  private function entityIsPreviewing(ContentEntityInterface $entity): bool {
    return $entity->isLatestTranslationAffectedRevision()
      && !$entity->isDefaultRevision()
      && !$entity->isNew();
  }

}
