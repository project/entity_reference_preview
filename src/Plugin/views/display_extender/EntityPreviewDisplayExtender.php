<?php

namespace Drupal\entity_reference_preview\Plugin\views\display_extender;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\display_extender\DisplayExtenderPluginBase;

/**
 * Entity preview display extender plugin.
 *
 * @ingroup views_display_extender_plugins
 *
 * @ViewsDisplayExtender(
 *   id = "entity_preview",
 *   title = @Translation("Entity preview display extender"),
 *   help = @Translation("Entity preview settings for this view."),
 * )
 */
class EntityPreviewDisplayExtender extends DisplayExtenderPluginBase {

  /**
   * Stores some state booleans to be sure a certain method got called.
   *
   * @var array
   */
  public $testState;

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['entity_reference_preview_enable'] = ['default' => 0];

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function optionsSummary(&$categories, &$options) {
    parent::optionsSummary($categories, $options);

    $categories['entity_reference_preview'] = [
      'title' => $this->t('Entity Preview'),
      'column' => 'second',
      'build' => [
        '#weight' => -100,
      ],
    ];

    $status_message = $this->options['entity_reference_preview_enable']
      ? $this->t('Enabled while preview mode is on.')
      : $this->t('Disabled.');
    $options['entity_reference_preview_enable'] = [
      'category' => 'entity_reference_preview',
      'title' => $this->t('Entity preview'),
      'value' => $status_message,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    $section = $form_state->get('section');
    switch ($section) {
      case 'entity_reference_preview_enable':
        $form['#title'] = $this->t('Entity preview');
        $form['entity_reference_preview_enable'] = [
          '#title' => $this->t('Entity preview'),
          '#type' => 'checkbox',
          '#description' => $this->t('Check to render the latest revisions on entities when the preview mode is active.'),
          '#default_value' => $this->options['entity_reference_preview_enable'],
        ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitOptionsForm(&$form, FormStateInterface $form_state) {
    parent::submitOptionsForm($form, $form_state);
    switch ($form_state->get('section')) {
      case 'entity_reference_preview_enable':
        $this->options['entity_reference_preview_enable'] = $form_state->getValue('entity_reference_preview_enable');
        break;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function defaultableSections(&$sections, $section = NULL) {
    $sections['entity_reference_preview_enable'] = ['entity_reference_preview_enable'];
  }

}
