<?php

namespace Drupal\entity_reference_preview\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_reference_preview\Events\PreviewNegotiationSubscriber;
use Drupal\entity_reference_preview\Form\PreviewActionsForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a preview detector block.
 *
 * @Block(
 *   id = "entity_reference_preview_preview_detector",
 *   admin_label = @Translation("Preview Detector"),
 *   category = @Translation("Editorial")
 * )
 */
class PreviewDetectorBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  private $formBuilder;

  /**
   * TRUE if the request is previewing values.
   *
   * @var bool
   */
  private $isPreviewing;

  /**
   * TRUE if the request is previewing values using the cookie detector.
   *
   * @var bool
   */
  private $previewingViaCookie;

  /**
   * Constructs a new PreviewDetectorBlock instance.
   *
   * @param array $configuration
   *   The plugin configuration, i.e. an array with configuration values keyed
   *   by configuration option name. The special key 'context' may be used to
   *   initialize the defined contexts by setting it to an array of context
   *   values keyed by context names.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Form\FormBuilderInterface $form_builder
   *   The form builder.
   * @param bool $is_previewing
   *   TRUE if the request is previewing values.
   * @param bool $previewing_via_cookie
   *   TRUE if the preview is via the cookie plugin.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $form_builder, bool $is_previewing, bool $previewing_via_cookie) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->formBuilder = $form_builder;
    $this->isPreviewing = $is_previewing;
    $this->previewingViaCookie = $previewing_via_cookie;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $request = $container->get('request_stack')->getCurrentRequest();
    $is_previewing = $request
      ->attributes
      ->get(PreviewNegotiationSubscriber::IS_PREVIEWING);
    $previewing_via_cookie = $request
      ->attributes
      ->get(PreviewNegotiationSubscriber::ACTIVE_DETECTOR) === 'cookie';
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('form_builder'),
      $is_previewing,
      $previewing_via_cookie
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    $condition = $account
      ->hasPermission('administer entity_reference_preview configuration');
    return AccessResult::allowedIf($condition);
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form_state = new FormState();
    return $this->formBuilder->buildForm(
      PreviewActionsForm::class,
      $form_state
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheContexts() {
    return array_merge(parent::getCacheContexts(), ['entity_reference_preview']);
  }

}
