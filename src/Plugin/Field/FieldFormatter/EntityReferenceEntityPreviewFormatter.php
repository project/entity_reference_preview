<?php

namespace Drupal\entity_reference_preview\Plugin\Field\FieldFormatter;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\entity_reference_preview\Entity\EntityStateManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'entity reference rendered entity' formatter.
 *
 * @FieldFormatter(
 *   id = "entity_reference_entity_view_preview",
 *   label = @Translation("Rendered entity (with preview)"),
 *   description = @Translation("Display the referenced entities rendered by
 *   entity_view() matching the page moderation state."),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class EntityReferenceEntityPreviewFormatter extends EntityReferenceEntityFormatter {

  /**
   * The entity state manager.
   *
   * @var \Drupal\entity_reference_preview\Entity\EntityStateManager
   */
  private $entityStateManager;

  /**
   * Detects the access to the draft indicator.
   *
   * @var bool
   */
  private $canSeeIndicator;

  /**
   * EntityReferenceEntityPreviewFormatter constructor.
   *
   * @param string $plugin_id
   *   The plugin_id for the formatter.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the formatter is associated.
   * @param array $settings
   *   The formatter settings.
   * @param string $label
   *   The formatter label display setting.
   * @param string $view_mode
   *   The view mode.
   * @param array $third_party_settings
   *   Any third party settings settings.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\EntityDisplayRepositoryInterface $entity_display_repository
   *   The entity display repository.
   * @param \Drupal\entity_reference_preview\Entity\EntityStateManager $entity_state_manager
   *   The entity state manager.
   * @param bool $can_see_indicator
   *   Boolean weather or not current user can see the indicator.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    $label,
    $view_mode,
    array $third_party_settings,
    LoggerChannelFactoryInterface $logger_factory,
    EntityTypeManagerInterface $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository,
    EntityStateManager $entity_state_manager,
    bool $can_see_indicator
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $label,
      $view_mode,
      $third_party_settings,
      $logger_factory,
      $entity_type_manager,
      $entity_display_repository
    );
    $this->entityStateManager = $entity_state_manager;
    $this->canSeeIndicator = $can_see_indicator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $can_see_indicator = $container->get('current_user')
      ->hasPermission('view entity_reference_preview indicator');
    $indicator_enabled = $container->get('config.factory')
      ->get('entity_reference_preview.settings')
      ->get('enableDraftIndicator');
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('logger.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get(EntityStateManager::class),
      $can_see_indicator && $indicator_enabled
    );
  }

  /**
   * {@inheritdoc}
   */
  public function prepareView(array $entities_items) {
    $target_type = $this->getFieldSetting('target_type');
    $entity_storage = $this->entityTypeManager->getStorage($target_type);
    $entity_storage instanceof RevisionableStorageInterface && $this->entityStateManager->isPreviewing()
      ? $this->preparePreviewView($entities_items)
      : parent::prepareView($entities_items);
  }

  /**
   * Prepares the field items with the entity property loading the revision.
   *
   * The appropriate revision is negotiated based on the latest revision, the
   * render language and its parents.
   *
   * @param array $entities_items
   *   The entity items containing the field items.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   *
   * @see \Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceFormatterBase::prepareView()
   */
  private function preparePreviewView(array $entities_items) {
    // Collect entity IDs to load. For performance, we want to use a single
    // "multiple entity load" to load all the entities for the multiple
    // "entity reference item lists" being displayed. We thus cannot use
    // \Drupal\Core\Field\EntityReferenceFieldItemList::referencedEntities().
    $target_ids = $revision_ids = [];
    $target_entities = [];
    $target_type = $this->getFieldSetting('target_type');
    foreach ($entities_items as $items) {
      foreach ($items as $item) {
        // To avoid trying to reload non-existent entities in
        // getEntitiesToView(), explicitly mark the items where $item->entity
        // contains a valid entity ready for display. All items are initialized
        // at FALSE.
        $item->_loaded = FALSE;
        if (!$this->needsEntityLoad($item)) {
          continue;
        }
        // Find the latest revisions.
        $target_ids[$item->target_id] = $item->target_id;
      }
    }
    if ($target_ids) {
      /** @var \Drupal\Core\Entity\RevisionableStorageInterface $entity_storage */
      $entity_storage = $this->entityTypeManager->getStorage($target_type);
      $revision_ids = $this->entityStateManager
        ->findRevisionIds($target_type, $target_ids);
      $target_entities = $entity_storage->loadMultipleRevisions($revision_ids);
    }

    // For each item, pre-populate the loaded entity in $item->entity, and set
    // the 'loaded' flag.
    foreach ($entities_items as $items) {
      foreach ($items as $item) {
        $vid = $revision_ids[$item->target_id];
        if (isset($target_entities[$vid])) {
          $item->entity = $target_entities[$vid];
          $item->_loaded = TRUE;
        }
        elseif ($item->hasNewEntity()) {
          $item->_loaded = TRUE;
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $build = parent::viewElements($items, $langcode);
    // If we are already previewing there is no need to show the indicator
    // either.
    if ($this->canSeeIndicator && !$this->entityStateManager->isPreviewing()) {
      $build = array_map(function (array $element) {
        $entity = &$element['#' . $element['#theme']];
        if (!$entity instanceof RevisionableInterface) {
          return $element;
        }
        $needs_indicator = !$this->entityStateManager->isLiveVersion($entity);
        $entity->_show_preview_indicator = $needs_indicator;
        $entity->addCacheTags(['erp_draft_indicator']);
        $indicator_cache_context = $needs_indicator
          ? 'entity_reference_preview.with_indicator'
          : 'entity_reference_preview.without_indicator';
        $entity->addCacheContexts([
          'user.permissions',
          'entity_reference_preview',
          $indicator_cache_context,
        ]);
        return $element;
      }, $build);
    }
    CacheableMetadata::createFromRenderArray($build)
      ->addCacheContexts(['user.permissions'])
      ->applyTo($build);
    return $build;
  }

}
