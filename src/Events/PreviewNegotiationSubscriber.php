<?php

namespace Drupal\entity_reference_preview\Events;

use Drupal\entity_reference_preview\PreviewDetectorPluginManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Negotiates the language candidates and sets them in the language manager.
 */
class PreviewNegotiationSubscriber implements EventSubscriberInterface {

  const IS_PREVIEWING = '_is_previewing';

  const ACTIVE_DETECTOR = '_active_detector';

  /**
   * The preview detector.
   *
   * @var \Drupal\entity_reference_preview\PreviewDetectorPluginManager
   */
  private $previewDetector;

  /**
   * PreviewNegotiationSubscriber constructor.
   *
   * @param \Drupal\entity_reference_preview\PreviewDetectorPluginManager $preview_detector
   *   The preview detector.
   */
  public function __construct(PreviewDetectorPluginManager $preview_detector) {
    $this->previewDetector = $preview_detector;
  }

  /**
   * Returns an array of event names this subscriber wants to listen to.
   *
   * @return array
   *   The event names to listen to
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::REQUEST => [['setPreview', 14]],
    ];
  }

  /**
   * Sets a flag in the request object if it belongs to a "latest" route.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The event.
   */
  public function setPreview(RequestEvent $event): void {
    $request = $event->getRequest();
    // Set the global request state in the request object for others to read.
    $active_detector = $this->previewDetector->activeDetector($request);
    $request->attributes->set(static::IS_PREVIEWING, (bool) $active_detector);
    $request->attributes->set(static::ACTIVE_DETECTOR, $active_detector);
  }

}
