<?php

namespace Drupal\entity_reference_preview\Form;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\entity_reference_preview\Events\PreviewNegotiationSubscriber;
use Drupal\entity_reference_preview\Plugin\PreviewDetector\CookiePreviewDetector;
use Drupal\entity_reference_preview\PreviewDetectorPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Entity Reference Preview form.
 */
class PreviewActionsForm extends FormBase {

  /**
   * The state detector plugin.
   *
   * @var \Drupal\entity_reference_preview\Plugin\PreviewDetector\CookiePreviewDetector
   */
  private $cookieDetector;

  /**
   * PreviewActionsForm constructor.
   *
   * @param \Drupal\entity_reference_preview\Plugin\PreviewDetector\CookiePreviewDetector $cookie_detector
   *   The state detector plugin.
   */
  public function __construct(CookiePreviewDetector $cookie_detector) {
    $this->cookieDetector = $cookie_detector;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $cookie_detector = $container->get(PreviewDetectorPluginManager::class)
      ->createInstance('cookie');
    return new static($cookie_detector);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_reference_preview_preview_actions';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $request = $this->getRequest();
    $is_previewing = $request
      ->attributes
      ->get(PreviewNegotiationSubscriber::IS_PREVIEWING);
    $active_preview_method = $request
      ->attributes
      ->get(PreviewNegotiationSubscriber::ACTIVE_DETECTOR);
    $previewing_cookie = $active_preview_method === 'cookie';
    $current_status = $is_previewing ? 'previewing' : 'stopped';
    $form['current'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#attributes' => ['class' => ['description']],
      '#value' => $this->getFeedbackMessage($previewing_cookie),
    ];
    $form['controls'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['erp-status']],
    ];
    $form['controls']['status'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('<span class="circle"></span> Current status is: %status.', [
        '%status' => $current_status,
      ]),
      '#attributes' => [
        'class' => [
          'preview-status-indicator',
          'preview--' . $current_status,
        ],
      ],
    ];
    $alternative_preview_method_active = $is_previewing && !$previewing_cookie;
    $form['controls']['actions'] = [
      '#type' => 'actions',
      '#access' => !$alternative_preview_method_active,
    ];
    $form['controls']['actions']['start'] = [
      '#type' => 'submit',
      '#name' => 'start',
      '#access' => !$previewing_cookie,
      '#value' => $this->t('Start'),
    ];
    $form['controls']['actions']['stop'] = [
      '#type' => 'submit',
      '#name' => 'stop',
      '#access' => $previewing_cookie,
      '#value' => $this->t('Stop'),
    ];
    $form['controls']['#attached'] = [
      'library' => ['entity_reference_preview/preview_controls'],
    ];
    $form['actions_info'] = [
      '#type' => 'html_tag',
      '#tag' => 'small',
      '#value' => $this->t(
        'Unable to show controls. Currently previewing via another method: %active',
        ['%active' => $active_preview_method]
      ),
      '#access' => $alternative_preview_method_active,
    ];

    (new CacheableMetadata())
      ->setCacheContexts(['entity_reference_preview'])
      ->applyTo($form);
    return $form;
  }

  /**
   * Get the feedback message for the preview status.
   *
   * @param bool $is_previewing
   *   TRUE indicates if the request is for preview.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The markup.
   */
  private function getFeedbackMessage(bool $is_previewing): TranslatableMarkup {
    $url = Url::fromRoute('entity_reference_preview.settings')->toString();
    return $is_previewing
      ? $this->t('All the entity references in this page using the formatter with preview are showing the latest version instead of the publicly available version.')
      : $this->t(
        'Enable reference preview to see the latest versions of the embedded referenced entities. When <a class="inline-link" href="@url">enabled</a>, users with access to the preview indicator will see a mark on embedded entities that have a draft available for preview.',
        ['@url' => $url]
      );
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $action = $form_state->getTriggeringElement()['#name'];
    if (!in_array($action, ['start', 'stop'])) {
      $form_state->setError($form, $this->t('Invalid action.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $action = $form_state->getTriggeringElement()['#name'];
    // Set the correct state value.
    $action === 'start'
      ? $this->cookieDetector->start()
      : $this->cookieDetector->stop();
    // Give feedback to the user.
    $message = $action === 'start'
      ? $this->t('Now previewing all referenced entities.')
      : $this->t('Previewing has been stopped. Reverted to normal entity reference behavior.');
    $this->messenger()->addStatus($message);
  }

}
