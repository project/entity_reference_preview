<?php

namespace Drupal\entity_reference_preview\Form;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Entity Reference Preview settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'entity_reference_preview_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['entity_reference_preview.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['draft_indicator'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Draft Indicator'),
      '#description' => $this->t('When enabled, users with enough permissions will be able to navigate the site and see a blue dot over the referenced entities that have ongoing drafts.'),
      '#default_value' => $this->config('entity_reference_preview.settings')->get('enableDraftIndicator'),
    ];
    $form['toolbar_integration'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Toolbar Integration'),
      '#description' => $this->t('When enabled, users with enough permissions will be able to have an additional tab in the toolbar for quick access to the preview controls. The tab also changes color if preview mode is on.'),
      '#default_value' => $this->config('entity_reference_preview.settings')->get('enableToolbarIntegration'),
    ];
    $form['attribution'] = [
      '#markup' => 'Icons made by <a href="https://www.flaticon.com/authors/freepik" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>',
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('entity_reference_preview.settings');
    $old_draft_indicator = (bool) $config->get('enableDraftIndicator');
    $new_draft_indicator = (bool) $form_state->getValue('draft_indicator');
    if ($new_draft_indicator !== $old_draft_indicator) {
      $config->set('enableDraftIndicator', $new_draft_indicator)->save();
      Cache::invalidateTags(['erp_draft_indicator']);
    }
    $old_toolbar_integration = (bool) $config->get('enableToolbarIntegration');
    $new_toolbar_integration = (bool) $form_state->getValue('toolbar_integration');
    if ($new_toolbar_integration !== $old_toolbar_integration) {
      $config->set('enableToolbarIntegration', $new_toolbar_integration)->save();
      Cache::invalidateTags(['erp_toolbar']);
    }
    parent::submitForm($form, $form_state);
  }

}
