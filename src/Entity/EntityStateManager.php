<?php

namespace Drupal\entity_reference_preview\Entity;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\ContentEntityStorageInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\RevisionableInterface;
use Drupal\Core\Entity\RevisionableStorageInterface;
use Drupal\Core\Entity\TranslatableRevisionableStorageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\entity_reference_preview\Events\PreviewNegotiationSubscriber;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Service that deals with the state of the entities.
 */
class EntityStateManager {

  /**
   * The entity state manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * The language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  private $languageManager;

  /**
   * Indicates if the state is for preview.
   *
   * @var bool
   */
  private $isPreviewing = FALSE;

  /**
   * EntityStateManager constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    LanguageManagerInterface $language_manager,
    RequestStack $request_stack
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
    $this->isPreviewing = $request_stack->getCurrentRequest()
      ->get(PreviewNegotiationSubscriber::IS_PREVIEWING, FALSE);
  }

  /**
   * Checks if the site is previewing.
   *
   * @return bool
   *   TRUE if the site is in preview mode.
   */
  public function isPreviewing(): bool {
    return $this->isPreviewing;
  }

  /**
   * Gets a list of entities and maybe changes them to their latest version.
   *
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   *   The entities.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   The user account to use for access checks.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   The entities in their latest revision (if appropriate).
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function maybeSwapEntities(array $entities, AccountInterface $account = NULL): array {
    if (empty($entities)) {
      return [];
    }
    if (!$this->isPreviewing) {
      return $entities;
    }
    // TODO: This is unideal, we should allow mixed entity types for this API.
    $entity_type_id = current($entities)->getEntityTypeId();
    $entity_ids = array_map(function (EntityInterface $entity) {
      return $entity->id();
    }, $entities);
    $entity_storage = $this->entityTypeManager->getStorage($entity_type_id);
    $revision_ids = $this->findRevisionIds($entity_type_id, $entity_ids);
    if (!$entity_storage instanceof RevisionableStorageInterface) {
      return $entity_storage->loadMultiple($revision_ids);
    }
    $swapped_entities = $entity_storage->loadMultipleRevisions($revision_ids);
    return array_reduce($swapped_entities, function (array $carry, RevisionableInterface $entity) use ($entities, $account) {
      $entity_id = $entity->id();
      /** @var \Drupal\Core\Entity\RevisionableInterface $old_entity */
      $old_entity = current(
        array_filter($entities, function (EntityInterface $e) use ($entity_id) {
          return $e->id() === $entity_id;
        })
      );
      if ($old_entity->getRevisionId() === $entity->getRevisionId()) {
        return $carry + [$entity->id() => $old_entity];
      }
      if (!$entity->access('view', $account)) {
        return $carry + [$entity->id() => $old_entity];
      }
      $entity->in_preview = TRUE;
      return $carry + [$entity->id() => $entity];
    }, []);
  }

  /**
   * Finds the revision ID for the latest revisions of some entities.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param \Drupal\Core\Entity\EntityInterface[] $entity_ids
   *   The entities.
   *
   * @return array
   *   The entity revisions.
   */
  public function findRevisionIds(string $entity_type_id, array $entity_ids): array {
    return array_map(function ($entity_id) use ($entity_type_id) {
      return $this->findRevisionId($entity_type_id, $entity_id);
    }, $entity_ids);
  }

  /**
   * Calculates the latest revision ID for the languages on the render node.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string|int $entity_id
   *   The entity ID.
   *
   * @return int|string|null
   *   The revision ID if any.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function findRevisionId(string $entity_type_id, $entity_id) {
    $entity_storage = $this->entityTypeManager->getStorage($entity_type_id);
    if (!$entity_storage instanceof RevisionableStorageInterface) {
      return $entity_id;
    }
    if (!$this->isPreviewing) {
      $entity = $entity_storage->load($entity_id);
      if ($entity) {
        return $entity->getRevisionId();
      }
      return NULL;
    }
    $ancestor_langcodes = $this->ancestorLangcodes($entity_type_id, $entity_id);
    // This only makes sense for revisionable entities that support translation.
    if (!$entity_storage instanceof TranslatableRevisionableStorageInterface) {
      return $entity_storage->getLatestRevisionId($entity_id);
    }
    foreach ($ancestor_langcodes as $ancestor_langcode) {
      $revision_id = $entity_storage->getLatestTranslationAffectedRevisionId(
        $entity_id,
        $ancestor_langcode
      );
      if ($revision_id) {
        return $revision_id;
      }
    }
    return $entity_storage->getLatestRevisionId($entity_id);
  }

  /**
   * Check if this is latest revision of the entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return bool
   *   TRUE is the entity is in its latest revision. FALSE otherwise.
   *
   * @see \Drupal\content_moderation\ModerationInformationInterface::hasPendingRevision()
   */
  public function isLiveVersion(EntityInterface $entity): bool {
    if (!$entity instanceof ContentEntityInterface) {
      return TRUE;
    }
    try {
      $storage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      return TRUE;
    }
    assert($storage instanceof ContentEntityStorageInterface);
    $latest_revision_id = $storage->getLatestTranslationAffectedRevisionId(
      $entity->id(),
      $entity->language()->getId()
    );
    $revision_id = $entity->getRevisionId();
    $is_default_revision = $entity->isDefaultRevision()
      && !$entity->isNewRevision()
      && $revision_id;
    $default_revision_id = $is_default_revision
      ? $revision_id
      : $this->getDefaultRevisionId($entity->getEntityTypeId(), $entity->id());
    if ($latest_revision_id != $default_revision_id && $latest_revision_id != '') {
      $latest_revision = $storage->loadRevision($latest_revision_id);
      assert($latest_revision instanceof ContentEntityInterface);
      return $latest_revision->wasDefaultRevision();
    }
    return TRUE;
  }

  /**
   * Returns the revision ID of the default revision for the specified entity.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param int $entity_id
   *   The entity ID.
   *
   * @return int
   *   The revision ID of the default revision, or NULL if the entity was
   *   not found.
   *
   * @see \Drupal\content_moderation\ModerationInformationInterface::getDefaultRevisionId()
   */
  private function getDefaultRevisionId($entity_type_id, $entity_id) {
    try {
      $storage = $this->entityTypeManager->getStorage($entity_type_id);
      $id_key = $this->entityTypeManager
        ->getDefinition($entity_type_id)
        ->getKey('id');
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      return NULL;
    }
    if (!$storage) {
      return NULL;
    }
    $result = $storage->getQuery()
      ->currentRevision()
      ->condition($id_key, $entity_id)
      // No access check is performed here since this is an API function and
      // should return the same ID regardless of the current user.
      ->accessCheck(FALSE)
      ->execute();
    return $result ? key($result) : NULL;
  }

  /**
   * Get the latest revision ID for the current entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return mixed
   *   The revision ID.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getLatestRevisionId(EntityInterface $entity) {
    $storage = $this->entityTypeManager->getStorage($entity->getEntityTypeId());
    if ($storage instanceof TranslatableRevisionableStorageInterface) {
      return $storage->getLatestRevisionId($entity->id());
    }
    assert($storage instanceof RevisionableStorageInterface);
    return $storage->getLatestRevisionId($entity->id());
  }

  /**
   * Find the ancestor language codes for a particular entity.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param string|null $entity_id
   *   The entity ID.
   *
   * @return array
   *   The list of language codes.
   */
  private function ancestorLangcodes(string $entity_type_id, $entity_id): array {
    $langcode = $this->languageManager->getCurrentLanguage()->getId();
    // Get the language candidates in order and add the default language to it.
    $ancestor_langcodes = $this->languageManager->getFallbackCandidates([
      'langcode' => $langcode,
      'operation' => 'find_entity_reference_revisions',
      'data' => ['entity_type' => $entity_type_id, 'entity_id' => $entity_id],
    ]);
    $default_langcode = $this->languageManager->getDefaultLanguage()->getId();
    if (!in_array($default_langcode, $ancestor_langcodes)) {
      $ancestor_langcodes = array_merge(
        $ancestor_langcodes,
        [$default_langcode => $default_langcode]
      );
    }
    return $ancestor_langcodes;
  }

}
