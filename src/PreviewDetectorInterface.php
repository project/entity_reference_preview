<?php

namespace Drupal\entity_reference_preview;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface for preview_detector plugins.
 */
interface PreviewDetectorInterface {

  /**
   * Returns the translated plugin label.
   *
   * @return string
   *   The translated title.
   */
  public function label(): string;

  /**
   * Checks if the current request is under preview.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   *
   * @return bool
   *   TRUE if the request is for a preview.
   */
  public function isPreviewing(Request $request): bool;

}
