<?php

namespace Drupal\entity_reference_preview;

use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\entity_reference_preview\Events\PreviewNegotiationSubscriber;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

/**
 * Toolbar integration for the preview detector.
 */
class PreviewDetectorToolbar {

  use StringTranslationTrait;

  /**
   * Route name for the page that allows altering the preview state.
   */
  const PREVIEW_CONTROLS_ROUTE_NAME = 'entity_reference_preview.controls';

  /**
   * Permission string to check for access to the control page.
   */
  const PREVIEW_CONTROLS_PERMISSION = 'request entity_reference_preview preview';

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private $request;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $user;

  /**
   * PreviewDetectorToolbar constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Session\AccountInterface $user
   *   The current user.
   */
  public function __construct(RequestStack $request_stack, AccountInterface $user) {
    $this->request = $request_stack->getCurrentRequest();
    $this->user = $user;
  }

  /**
   * Alter the render array toolbar integration.
   *
   * @param array $items
   *   The render array for the toolbar integration.
   */
  public function build(array &$items): void {
    $active_detector = $this->request
      ->attributes
      ->get(PreviewNegotiationSubscriber::ACTIVE_DETECTOR);
    $current_status = $active_detector ? 'previewing' : 'stopped';

    $items['preview_controls'] = [
      '#type' => 'toolbar_item',
      'tab' => [
        '#type' => 'link',
        '#title' => $this->t('Preview'),
        '#url' => Url::fromRoute('system.admin'),
        '#attributes' => [
          'class' => [
            'toolbar-icon',
            'preview--' . ($active_detector ? 'started' : 'stopped'),
          ],
        ],
      ],
      'tray' => [
        '#type' => 'container',
        '#attributes' => ['class' => ['erp-status']],
        'status' => [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#value' => $this->t('<span class="circle"></span> Current status is: %status.', [
            '%status' => $current_status,
          ]),
          '#attributes' => [
            'class' => [
              'preview-status-indicator',
              'preview--' . $current_status,
            ],
          ],
        ],
        'link' => $this->getControlsLink($active_detector),
      ],
      '#attached' => [
        'library' => [
          'entity_reference_preview/preview_controls',
        ],
      ],
      '#wrapper_attributes' => [
        'id' => 'entity-reference-preview-controls-tab',
      ],
    ];
  }

  /**
   * Get the controls link.
   *
   * This is the link that will take you to the controls page if appropriate.
   *
   * @param string $active_detector
   *   The name of the active detector.
   *
   * @return array
   *   The render array for the active detector link.
   */
  protected function getControlsLink($active_detector): array {
    $has_permission = $this->user
      ->hasPermission(static::PREVIEW_CONTROLS_PERMISSION);
    $controls = [
      '#type' => 'link',
      '#title' => $this->t('Change'),
      '#url' => Url::fromRoute(static::PREVIEW_CONTROLS_ROUTE_NAME),
      '#access' => $has_permission,
    ];
    try {
      $controls['#url']
        ->setRouteParameter(
          'destination',
          Url::createFromRequest($this->request)->toString()
        );
    }
    catch (ResourceNotFoundException $exception) {
      // If the request is 404 we can ignore it.
    }
    if ($active_detector && $active_detector !== 'cookie') {
      $message = $active_detector === 'rendered_entity'
        ? 'The preview state is active because you are seeing the latest version of an entity under moderation.'
        : 'The preview state is triggered by an external action.';
      $controls = [
        '#markup' => $this->t('<a href="#" title="@message">?</a>', ['@message' => $message]),
      ];
    }
    return $controls;
  }

}
