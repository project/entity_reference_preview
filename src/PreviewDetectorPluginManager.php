<?php

namespace Drupal\entity_reference_preview;

use Drupal\Component\Plugin\FallbackPluginManagerInterface;
use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Symfony\Component\HttpFoundation\Request;

/**
 * PreviewDetector plugin manager.
 */
class PreviewDetectorPluginManager extends DefaultPluginManager implements FallbackPluginManagerInterface {

  /**
   * Constructs PreviewDetectorPluginManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/PreviewDetector',
      $namespaces,
      $module_handler,
      'Drupal\entity_reference_preview\PreviewDetectorInterface',
      'Drupal\entity_reference_preview\Annotation\PreviewDetector'
    );
    $this->setCacheBackend($cache_backend, 'preview_detector_plugins');
  }

  /**
   * {@inheritdoc}
   */
  public function getFallbackPluginId($plugin_id, array $configuration = []) {
    return 'rendered_entity';
  }

  /**
   * Checks if the request is for a preview state.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The global request object.
   * @param array $configuration
   *   The configuration for the preview detectors.
   *
   * @return bool
   *   TRUE if the request is for a preview.
   */
  public function isPreviewing(Request $request, array $configuration = []): bool {
    return (bool) $this->activeDetector($request, $configuration);
  }

  /**
   * Gets the active detector ID, if any, for the current request.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The global request object.
   * @param array $configuration
   *   The configuration for the preview detectors.
   *
   * @return string|null
   *   The plugin ID that is triggering the preview state.
   */
  public function activeDetector(Request $request, array $configuration = []) {
    $instances = array_map(function (array $definition) use ($configuration) {
      return $this->createInstance($definition['id'], $configuration);
    }, $this->getDefinitions());
    // Return true if any of the plugins is true.
    return array_reduce(
      $instances,
      static function ($detected, PreviewDetectorInterface $detector) use ($request) {
        assert($detector instanceof PluginInspectionInterface);
        if ($detected) {
          return $detected;
        }
        return $detector->isPreviewing($request)
          ? $detector->getPluginId()
          : NULL;
      },
      NULL
    );
  }

}
