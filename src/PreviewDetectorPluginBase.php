<?php

namespace Drupal\entity_reference_preview;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for preview_detector plugins.
 */
abstract class PreviewDetectorPluginBase extends PluginBase implements PreviewDetectorInterface {

  /**
   * {@inheritdoc}
   */
  public function label(): string {
    // Cast the label to a string since it is a TranslatableMarkup object.
    return (string) $this->pluginDefinition['label'];
  }

}
